#!/bin/bash

echo "FILEBEAT SETUP TO GET APACHE LOGS"

echo "THIS WILL WORK IN UBUNTU ONLY"
KEY_PAIR="eks.pem"
SERVER="ubuntu@54.251.216.167"
ssh -i $KEY_PAIR $SERVER <<-'ENDSSH'
echo "FILEBEAT INSTALLATION"
sudo su -
#apt update
#apt upgrade -y
#apt install nginx -y
#systemctl enable nginx
#systemctl start nginx
#echo "FILEBEAT INSTALLATION"

wget https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.11.2-amd64.deb
dpkg -i filebeat-7.11.2-amd64.deb

echo "FILEBEAT CONFIGURATION"

cd /etc/filebeat/
cp filebeat.yml filebeat.yml_bkup
>filebeat.yml
cat >> filebeat.yml <<-EOF
filebeat.inputs:
- type: log
  enabled: true
  paths:
    - /opt/tomcat/logs/catalina.2021-03-20.log
output.elasticsearch:
  hosts: ["172.31.18.196:9200"]
  enabled: true
  index: "stgtomcat-%{[agent.version]}-%{+yyyy.MM}"
setup.kibana:
  host: "http://172.31.18.196:5601"
setup.dashboards.enabled: true
setup.dashboards.index: "stgtomcat-*"
setup.template.enabled: true
setup.template.name: "stgtomcat"
setup.template.pattern: "stgtomcat-*"
setup.template.overwrite: true
setup.template.settings:
  index:
    number_of_shards: 1
setup.ilm.enabled: false
processors:
  - add_host_metadata:
      when.not.contains.tags: forwarded
  - add_cloud_metadata: ~
  - add_docker_metadata: ~
  - add_kubernetes_metadata: ~
EOF

systemctl enable filebeat
systemctl start filebeat
ENDSSH
