#!/bin/bash
echo "THIS WILL WORK IN UBUNTU ONLY"
KEY_PAIR="eks.pem"
SERVER="ubuntu@54.251.216.167"
ssh -i $KEY_PAIR $SERVER <<-'ENDSSH'

#PREREQUISITES
#login: user with root privileges
#OS: Ubuntu 18.04
#RAM: 4GB
#CPU: 2
#Java: Oracle JDK 8 version

sudo apt update
sudo apt-get upgrade -y
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt install openjdk-8-jdk -y
sudo su -
echo "ELASTIC SEARCH INSTALLATION"
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.11.2-amd64.deb
dpkg -i elasticsearch-7.11.2-amd64.deb

echo "LOGSTASH INSTALLATION"
wget https://artifacts.elastic.co/downloads/logstash/logstash-7.11.2-amd64.deb
dpkg -i logstash-7.11.2-amd64.deb

echo "KIBANA INSTALLATION"
wget https://artifacts.elastic.co/downloads/kibana/kibana-7.11.2-amd64.deb
dpkg -i kibana-7.11.2-amd64.deb

echo "ELASTICSEARCH CONFIGURATION"
cd /etc/elasticsearch/
echo "network.host: 0.0.0.0" >> elasticsearch.yml
echo "http.port: 9200" >> elasticsearch.yml
echo "discovery.type: single-node" >> elasticsearch.yml
systemctl enable elasticsearch
systemctl start elasticsearch

echo "KIBANA CONFIGURATION"
cd /etc/kibana/
echo "server.port: 5601" >> kibana.yml
echo "server.host: "0.0.0.0"" >> kibana.yml
echo "elasticsearch.hosts: ["http://localhost:9200"]" >> kibana.yml
systemctl enable kibana
systemctl start kibana
echo "CHECK ip:5601 in browser"
ENDSSH
