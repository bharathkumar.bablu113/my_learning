#!/bin/bash

KEY_PAIR="eks.pem"
SERVER="ubuntu@54.251.216.167"
ssh -i $KEY_PAIR $SERVER <<-'ENDSSH'

#Step 1 — Install Java
#First, update your apt package index:
sudo su -
sudo apt update
#Then install the Java Development Kit package with apt:
sudo apt install default-jdk -y
#Step 2 — Create Tomcat User
#First, create a new tomcat group:

sudo groupadd tomcat
#Next, create a new tomcat user. We’ll make this user a member of the tomcat group, with a home directory of /opt/tomcat (where we will install Tomcat), and with a shell of /bin/false (so nobody can log into the account):
sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
#Step 3 — Install Tomcat
#Next, change to the /tmp directory on your server. This is a good directory to download ephemeral items, like the Tomcat tarball, which we won’t need after extracting the Tomcat contents:
cd /tmp
#Use curl to download the link that you copied from the Tomcat website:

curl -O https://mirrors.estointernet.in/apache/tomcat/tomcat-10/v10.0.4/bin/apache-tomcat-10.0.4.tar.gz
#We will install Tomcat to the /opt/tomcat directory. Create the directory, then extract the archive to it with these commands:
sudo mkdir /opt/tomcat 
sudo tar xzvf apache-tomcat-*tar.gz -C /opt/tomcat --strip-components=1
#Step 4 — Update Permissions
#Change to the directory where we unpacked the Tomcat installation:
cd /opt/tomcat
#Give the tomcat group ownership over the entire installation directory:
sudo chgrp -R tomcat /opt/tomcat
#Next, give the tomcat group read access to the conf directory and all of its contents, and execute access to the directory itself:
sudo chmod -R g+r conf
sudo chmod g+x conf
#Make the tomcat user the owner of the webapps, work, temp, and logs directories:
sudo chown -R tomcat webapps/ work/ temp/ logs/

#Step 5 — Create a systemd Service File
#With this piece of information, we can create the systemd service file. Open a file called tomcat.service in the /etc/systemd/system directory by typing:
sudo cat > /etc/systemd/system/tomcat.service <<-EOF
[Unit]
Description=Apache Tomcat Web Application Container
After=network.target

[Service]
Type=forking

Environment=JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64
Environment=CATALINA_PID=/opt/tomcat/temp/tomcat.pid
Environment=CATALINA_HOME=/opt/tomcat
Environment=CATALINA_BASE=/opt/tomcat
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=/opt/tomcat/bin/startup.sh
ExecStop=/opt/tomcat/bin/shutdown.sh

User=tomcat
Group=tomcat
UMask=0007
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target

EOF

#Next, reload the systemd daemon so that it knows about our service file:
sudo systemctl daemon-reload
#Start the Tomcat service by typing:
sudo systemctl start tomcat
sudo systemctl enable tomcat

ENDSSH
ssh -i eks.pem $SERVER
